<?php namespace App\Models;
use CodeIgniter\Model;
class StudentsModel extends Model
{
    protected $table = 'students'; //таблица, связанная с моделью
    protected $allowedFields = ['fio', 'ID_group', 'ID_student','picture_url'];
    protected $primaryKey = 'ID_student';
    public function getStudents($id = null, $search = '')
    {
        if (!isset($id)) {
            return $this->select('*')->like('fio', $search,'both', true, true);
        }
        return $this->where(['ID_student' => $id])->first();
    }

}
