<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div style="
background: url(https://st.volga.news/image/w1300/h900/max/70a390ee-64dc-4a36-8a31-0176944b537f.jpg) top center no-repeat;
padding: 170px 0;
background-size: cover;
">
    <div class="container" style="color: white;">
        <div class="col-6 p-4" style="background-color: rgba(0,0,0,.2)">
            <h1>Посещаемость студентов</h1>
            <div>Это приложение поможет вести учёт посещаемости</div>
            <?php if (!$ionAuth->loggedIn()): ?>
                <a class="px-4 py-2 link-primary mt-3"
                   style="display: inline-block; color: white; background: #CA1D20; border: none; border-radius: 5px;"
                   href="<?= base_url() ?>/auth/login">Вход
                </a>
            <?php endif ?>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

