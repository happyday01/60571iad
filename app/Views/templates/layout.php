<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Просмотр посещаемости</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<style>
    a:hover {
        text-decoration: none;
    }
    .pagination .page-item.active .page-link{
        background-color: #CA1D20;
        border-color: #CA1D20;
        color: white;
    }
    .pagination .page-item .page-link{
        color: #CA1D20;
        border-color: #CA1D20;
    }
</style>
<body>
<header class="header">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background: #CA1D20;">
        <a class="navbar-brand p-0" href="<?= base_url() ?>">
            <img src="<?= base_url() ?>/img/logo.svg" alt="">
        </a>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault" style="display: inline-flex;">
            <ul class="navbar-nav mr-auto" style="list-style-type: none;display: flex; text-decoration: none;">
                <li class="nav-item dropdown" style="margin-left: 25px;">
                    <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="true">Посещаемость
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="<?= base_url() ?>/students">Все студенты</a>
                        <a class="dropdown-item" href="<?= base_url() ?>/students/store">Создать студента</a>
                    </div>
                </li>
                <li class="nav-item dropdown" style="margin-left: 25px;">
                    <a class="nav-link dropdown-toggle active" id="dropdown01" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="true">Мой профиль</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="#">Мой профиль</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav">
                <?php if ($ionAuth->loggedIn()): ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="true">
                            <span class="fas fa fa-user-alt"></span>
                            <?php echo $ionAuth->user()->row()->email; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="<?= base_url() ?>/auth/logout">
                                <span class="fas fa fa-sign-in-alt"></span>
                                Выход
                            </a>
                        </div>
                    </li>
                <?php endif ?>
            </ul>
        </div>
    </nav>
</header>
<main role="main" style="margin-top: 96px;">
    <?= $this->renderSection('content') ?>
</main>
<footer class="py-2 text-center">
    © Ivanchuk Alexandra 2021 |
    <a href="<?php echo base_url(); ?>/pages/view/agreement">Пользовательское соглашение</a>
</footer>
<script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>
</body>
</html>