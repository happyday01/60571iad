<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container pt-4">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <?= form_open_multipart('/students/store'); ?>
            <div class="form-group">
                <label for="fio">Имя</label>
                <input type="text" class="form-control <?= ($validation->hasError('fio')) ? 'is-invalid' : ''; ?>"
                       name="fio"
                       id="fio"
                       value="<?= old('fio'); ?>">
                <div class="invalid-feedback">
                    <?= $validation->getError('fio') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="ID_group">ID группы</label>
                <input type="text" class="form-control <?= ($validation->hasError('ID_group')) ? 'is-invalid' : ''; ?>"
                       name="ID_group"
                       id="ID_group"
                       value="<?= old('ID_group'); ?>">
                <div class="invalid-feedback">
                    <?= $validation->getError('ID_group') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="picture">Изображение</label>
                <input type="file"
                       class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>"
                       name="picture"
                       id="picture">
                <div class="invalid-feedback">
                    <?= $validation->getError('picture') ?>
                </div>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-danger px-5" name="submit">Создать</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

