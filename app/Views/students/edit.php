<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container pt-4">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <?= form_open_multipart('students/update'); ?>
                <input type="hidden" name="id" value="<?= $students["ID_student"] ?>">
                <div class="form-group">
                    <label for="fio">Имя</label>
                    <input type="text" class="form-control <?= ($validation->hasError('fio')) ? 'is-invalid' : ''; ?>"
                           name="fio"
                           id="fio"
                           value="<?= $students["fio"]; ?>">
                    <div class="invalid-feedback">
                        <?= $validation->getError('fio') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ID_group">ID группы</label>
                    <input type="text"
                           class="form-control <?= ($validation->hasError('ID_group')) ? 'is-invalid' : ''; ?>"
                           name="ID_group"
                           id="ID_group"
                           value="<?= $students["ID_group"] ?>">
                    <div class="invalid-feedback">
                        <?= $validation->getError('ID_group') ?>
                    </div>

                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-danger px-5" name="submit">Сохранить</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?= $this->endSection() ?><?php
