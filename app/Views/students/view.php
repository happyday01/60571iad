<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container pt-4">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($students)) : ?>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5>Студент: <?= esc($students['fio']); ?></h5>
                        <div>ID группы: <?= esc($students['ID_group']); ?></div>
                        <div>ID студента: <?= esc($students['ID_student']); ?></div>
                        <div class="d-flex justify-content-start align-items-center mb-2">
                            <div class="mr-2">Текущий рейтинг:</div>
                            <span class="badge badge-info">199</span>
                        </div>
                        <a href="<?= base_url() ?>/students/edit/<?= esc($students['ID_student']); ?>"
                           class="btn btn-warning btn-sm">Редактировать</a>
                        <a href="<?= base_url() ?>/students/delete/<?= esc($students['ID_student']); ?>"
                           class="btn btn-danger btn-sm">Удалить</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<?= $this->endSection() ?>

