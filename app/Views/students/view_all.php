<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container pt-4">
    <h2 class="mb-3">Все студенты</h2>
    <div class="d-flex justify-content-between" style="margin-bottom: 20px;">
        <?= $pager->links('group1', 'my_page') ?>
        <?= form_open('students', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-danger" type="submit">На странице</button>
        </form>
        <?= form_open('students', ['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="ФИО" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-danger" type="submit">Найти</button>
        </form>
    </div>

    <?php if (!empty($students) && is_array($students)) : ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">ФИО</th>
                <th scope="col">Группа</th>
                <th scope="col">ID студента</th>
                <th scope="col">Управление</th>
                <th scope="col">Рейтинг</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($students as $item): ?>
                <tr>
                    <td>
                        <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img"
                             alt="<?= esc($item['fio']); ?>">
                    </td>
                    <td><?= esc($item['ID_group']); ?></td>
                    <td><?= esc($item['ID_student']); ?></td>
                    <td>
                        <a href="<?= base_url() ?>/students/view/<?= esc($item['ID_student']); ?>"
                           class="btn btn-primary btn-sm">Просмотреть</a>
                        <a href="<?= base_url() ?>/students/edit/<?= esc($item['ID_student']); ?>"
                           class="btn btn-warning btn-sm">Редактировать</a>
                        <a href="<?= base_url() ?>/students/delete/<?= esc($item['ID_student']); ?>"
                           class="btn btn-danger btn-sm">Удалить</a>
                    </td>
                    <td>199</td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <div class="text-center">
            <p>Студенты не найдены </p>
            <a class="btn btn-primary btn-lg" href="<?= base_url() ?>/students/create">
                <span class="fas fa-tachometer-alt" style="color:white"></span>Создать студента
            </a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
