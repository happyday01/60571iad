<?php namespace App\Controllers;

use App\Models\StudentsModel;
use Aws\S3\S3Client;

class Students extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form', 'url']);
        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        } else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '2'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search'))) {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        } else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;
        $model = new StudentsModel();
        $data ['students'] = $model->getStudents(null, $search)->paginate($per_page, 'group1');
        $data['pager'] = $model->pager;
        echo view('students/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new StudentsModel();
        $data ['students'] = $model->getStudents($id);
        echo view('students/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('/students/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);
        if ($this->request->getMethod() === 'post' && $this->validate([
                'fio' => 'required|min_length[3]|max_length[255]',
                'ID_group' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new StudentsModel();
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save([
                'fio' => $this->request->getPost('fio'),
                'ID_group' => $this->request->getPost('ID_group'),
            ]);
            session()->setFlashdata('message', lang('Curating.rating_create_success'));
            return redirect()->to('/students');
        } else {
            return redirect()->to('/students/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new StudentsModel();
        helper(['form']);
        $data ['students'] = $model->getStudents($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('students/edit', $this->withIon($data));

    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/students/edit/' . $this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'fio' => 'required|min_length[3]|max_length[255]',
                'ID_group' => 'required',
            ])) {
            $model = new StudentsModel();
            $model->save([

                'fio' => $this->request->getPost('fio'),
                'ID_group' => $this->request->getPost('ID_group'),
                'ID_student' => $this->request->getPost('id'),
            ]);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));
            return redirect()->to('/students');
        } else {
            return redirect()->to('/students/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new StudentsModel();
        $model->delete($id);
        return redirect()->to('/students');
    }
}
