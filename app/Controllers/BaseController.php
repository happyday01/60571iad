<?php
namespace App\Controllers;

use App\Services\GoogleClient;
use App\Services\IonAuthGoogle;
use CodeIgniter\Controller;

class BaseController extends Controller
{

    protected $helpers = [];
    protected $ionAuth;
    protected $google_client;

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {

        parent::initController($request, $response, $logger);

        $this->google_client = new GoogleClient();
        $this->ionAuth = new IonAuthGoogle();
    }
    protected function withIon(array $data = [])
    {
        $data['ionAuth'] = $this->ionAuth;
        $data['authUrl'] = $this->google_client->getGoogleClient()->createAuthUrl();
        return $data;
    }
}