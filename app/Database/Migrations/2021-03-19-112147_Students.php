<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Students extends Migration
{
	public function up()
    {


        // departments
        if (!$this->db->tableexists('departments')) {
            // Setup Keys
            $this->forge->addkey('ID_department', TRUE);

            $this->forge->addfield(array(
                'ID_department' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'Name_department' => array('type' => 'TEXT', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('departments', TRUE);
        }

        // specialisations
        if (!$this->db->tableexists('specialisations')) {
            // Setup Keys
            $this->forge->addkey('ID_specialisation', TRUE);

            $this->forge->addfield(array(
                'ID_specialisation' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'Name_specialisation' => array('type' => 'TEXT', 'null' => FALSE),
                'ID_department' => array('type' => 'INT', 'null' => TRUE),
                'Cod' => array('type' => 'VARCHAR', 'constraint' => '8', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('ID_department', 'departments', 'ID_department', 'RESTRICT', 'RESRICT');
            // create table
            $this->forge->createtable('specialisations', TRUE);
        }

        // professors
        if (!$this->db->tableexists('professors')) {
            // Setup Keys
            $this->forge->addkey('ID_professor', TRUE);

            $this->forge->addfield(array(
                'ID_professor' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'Name_professor' => array('type' => 'TEXT', 'null' => FALSE),
                'ID_department' => array('type' => 'INT', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('ID_department', 'departments', 'ID_department', 'RESTRICT', 'RESRICT');
            // create table
            $this->forge->createtable('professors', TRUE);
        }

        // group
        if (!$this->db->tableexists('group')) {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'Number' => array('type' => 'TEXT', 'null' => FALSE),
                'ID_specialisation' => array('type' => 'INT', 'null' => TRUE),
                'Mentor' => array('type' => 'TEXT', 'null' => TRUE),
                'ID_professor' => array('type' => 'INT', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('ID_specialisation', 'specialisations', 'ID_specialisation', 'RESTRICT', 'RESRICT');
            $this->forge->addForeignKey('ID_professor', 'professors', 'ID_professor', 'RESTRICT', 'RESRICT');
            // create table
            $this->forge->createtable('group', TRUE);
        }



        // students
        if (!$this->db->tableexists('students')) {
            // Setup Keys
            $this->forge->addkey('ID_student', TRUE);

            $this->forge->addfield(array(
                'ID_student' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'FIO' => array('type' => 'TEXT', 'null' => FALSE),
                'ID_group' => array('type' => 'INT', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('ID_group', 'group', 'ID', 'RESTRICT', 'RESRICT');
            // create table
            $this->forge->createtable('students', TRUE);
        }




    }

	public function down()
	{
		//

        $this->forge->droptable('students');
        $this->forge->droptable('group');
        $this->forge->droptable('professors');
        $this->forge->droptable('specialisations');
        $this->forge->droptable('departments');



	}
}
