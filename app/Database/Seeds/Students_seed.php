<?php
namespace Services\Database\Seeds;

use CodeIgniter\Database\Seeder;

class students_seed extends Seeder
{
    public function run()
    {

            $data = [

                'Name_department' => 'Автоматики и компьютерных систем ',
            ];
        $this->db->table('departments')->insert($data);

            $data = [

            'Name_department' => 'Экспериментальной физики ',
        ];
        $this->db->table('departments')->insert($data);


        $data = [

            'Name_specialisation'=> 'Информатика и вычислительная техника',
            'ID_department'=>'1',
            'Cod' => '09.03.01',
        ];
        $this->db->table('specialisations')->insert($data);

        $data = [

            'Name_specialisation'=> 'Программная инженерия',
            'ID_department'=>'1',
            'Cod' => '09.03.01',
        ];
        $this->db->table('specialisations')->insert($data);




        $data = [

            'Name_professor'=> 'Запевалов А.В.',
            'ID_department'=>'1',
        ];
        $this->db->table('professors')->insert($data);

        $data = [

            'Name_professor'=> 'Паук Е.Н.',
            'ID_department'=>'1',
        ];
        $this->db->table('professors')->insert($data);


                $data = [

                    'Number' => '601-01',
                    'ID_specialisation'=>'1',
                    'Mentor'=>'NULL',
                    'ID_professor'=>'1',
                ];
        $this->db->table('group')->insert($data);

        $data = [

            'Number' => '602-01',
            'ID_specialisation'=>'1',
            'Mentor'=>'NULL',
            'ID_professor'=>'1',
        ];
        $this->db->table('group')->insert($data);

        $data = [

            'FIO' => 'Ивко А.В.',
            'ID_group'=>'3',
        ];
        $this->db->table('students')->insert($data);

        $data = [

            'FIO' => 'Биляева В.Д.',
            'ID_group'=>'2',

        ];
        $this->db->table('students')->insert($data);
        $data = [

            'FIO' => 'Бирюкова Е.Д.',
            'ID_group'=>'2',

        ];
        $this->db->table('students')->insert($data);
        $data = [

            'FIO' => 'Бекчив В.Н.',
            'ID_group'=>'2',

        ];
        $this->db->table('students')->insert($data);








    }
}
