-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Фев 24 2021 г., 19:46
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571iad`
--

-- --------------------------------------------------------

--
-- Структура таблицы `departments`
--

CREATE TABLE `departments` (
  `ID_department` int NOT NULL,
  `Name_department` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `departments`
--

INSERT INTO `departments` (`ID_department`, `Name_department`) VALUES
(1, 'Автоматики и компьютерных систем '),
(2, 'Экспериментальной физики '),
(3, 'Радиоэлектроники и электроэнергетики '),
(4, 'Прикладной математики '),
(5, 'Высшей математики '),
(6, 'Информатики и вычислительной техники '),
(7, 'Автоматизированных систем обработки информации и управления'),
(8, 'Строительных технологий и конструкций');

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `ID` int NOT NULL,
  `Number` text NOT NULL,
  `ID_specialisation` varchar(8) DEFAULT NULL,
  `Mentor` text,
  `ID_professor` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`ID`, `Number`, `ID_specialisation`, `Mentor`, `ID_professor`) VALUES
(1, '601-01', '17', NULL, NULL),
(2, '601-01(м)', '1', NULL, NULL),
(3, '602-01', '16', NULL, NULL),
(4, '602-01(м)', '15', NULL, NULL),
(5, '603-01', '5', NULL, NULL),
(6, '603-02', '5', NULL, NULL),
(7, '603-01(м)', '4', NULL, NULL),
(8, '604-01', '7', NULL, NULL),
(9, '604-01(м)', '6', NULL, NULL),
(10, '605-01', '3', NULL, NULL),
(11, '605-01(м)', '2', NULL, NULL),
(12, '605-71', '3', 'Тараканов Д.В.', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `professors`
--

CREATE TABLE `professors` (
  `ID_professor` int NOT NULL,
  `Name_professor` text NOT NULL,
  `ID_department` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `professors`
--

INSERT INTO `professors` (`ID_professor`, `Name_professor`, `ID_department`) VALUES
(2, 'Запевалов А.В.', 1),
(3, 'Запевалова Л.Ю.', 1),
(4, 'Паук Е.Н.', 1),
(5, 'Тараканов Д.В.', 1),
(6, 'Даниленко И.Н.', 1),
(7, 'Гришмановский П.В.', 1),
(8, 'Гришмановская О.Н.', 1),
(9, 'Кузин Д.А.', 1),
(10, 'Назаров Е.В.', 1),
(11, 'Кривицкая М.А.', 1),
(12, 'Брагинский М.Я.', 1),
(13, 'Кошкин С.С.', 1),
(14, 'Казаковцева Е.А.', 1),
(15, 'Емельянов С.Н.', 1),
(16, 'Волков А.В.', 1),
(17, 'Золотарева Н.С.', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `specialisations`
--

CREATE TABLE `specialisations` (
  `ID_specialisation` varchar(8) NOT NULL,
  `Name_specialisation` text NOT NULL,
  `ID_department` int DEFAULT NULL,
  `Cod` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `specialisations`
--

INSERT INTO `specialisations` (`ID_specialisation`, `Name_specialisation`, `ID_department`, `Cod`) VALUES
('1', 'Прикладная математика и информатика(м)', 4, '01.04.02'),
('10', 'Программная инженерия\r\n', 1, '09.03.04'),
('11', 'Информационные системы и технологии', 6, '09.03.02'),
('12', 'Информатика и вычислительная техника\r\n', 6, '09.03.01'),
('13', 'Строительство(м)', 8, '08.04.01'),
('14', 'Строительство', 8, '08.03.01'),
('15', 'Физика(м)', 2, '03.04.02'),
('16', 'Физика', 2, '03.03.02'),
('17', 'Прикладная математика и информатика\r\n', 4, '01.03.02'),
('2', 'Управление в технических системах(м)', 1, '27.04.04'),
('3', 'Управление в технических системах\r\n', 1, '27.03.04'),
('4', 'Электроэнергетика и электротехника(м)', 3, '13.04.02'),
('5', 'Электроэнергетика и электротехника\r\n', 3, '13.03.02'),
('6', 'Инфокоммуникационные технологии и системы связи(м)', 3, '11.04.02'),
('7', 'Инфокоммуникационные технологии и системы связи', 3, '11.03.02'),
('8', 'Информационные системы и технологии(м)', 6, '09.04.02'),
('9', 'Информатика и вычислительная техника(м)', 6, '09.04.01');

-- --------------------------------------------------------

--
-- Структура таблицы `students`
--

CREATE TABLE `students` (
  `ID_student` int NOT NULL,
  `FIO` text NOT NULL,
  `ID_group` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `students`
--

INSERT INTO `students` (`ID_student`, `FIO`, `ID_group`) VALUES
(1, 'Архипов А.С.', 12),
(2, 'Акобян Т.С.', 12),
(3, 'Батютенко М.С.', 12),
(4, 'Валиев Р.Ф.', 12),
(5, 'Ганиев Н.А.', 12),
(6, 'Горшкова А.С.', 12),
(7, 'Гумерова А.А.', 12),
(8, 'Еременко М.Ю.', 12),
(9, 'Иванчук А.Д.', 12),
(10, 'Иксанова И.Р.', 12),
(11, 'Кожихова К.Е.', 12),
(12, 'Кузнецов А.А.', 12),
(13, 'Коростелев Д.Н.', 12),
(14, 'Колмагоров А.А.', 12),
(15, 'Меркер Т.З.', 12);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`ID_department`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_professor` (`ID_professor`),
  ADD KEY `ID_specialisation` (`ID_specialisation`);

--
-- Индексы таблицы `professors`
--
ALTER TABLE `professors`
  ADD PRIMARY KEY (`ID_professor`),
  ADD KEY `ID_department` (`ID_department`);

--
-- Индексы таблицы `specialisations`
--
ALTER TABLE `specialisations`
  ADD PRIMARY KEY (`ID_specialisation`),
  ADD KEY `ID_department` (`ID_department`);

--
-- Индексы таблицы `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`ID_student`),
  ADD KEY `ID_group` (`ID_group`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `departments`
--
ALTER TABLE `departments`
  MODIFY `ID_department` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `professors`
--
ALTER TABLE `professors`
  MODIFY `ID_professor` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `students`
--
ALTER TABLE `students`
  MODIFY `ID_student` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_ibfk_2` FOREIGN KEY (`ID_professor`) REFERENCES `professors` (`ID_professor`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `groups_ibfk_3` FOREIGN KEY (`ID_specialisation`) REFERENCES `specialisations` (`ID_specialisation`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `professors`
--
ALTER TABLE `professors`
  ADD CONSTRAINT `professors_ibfk_2` FOREIGN KEY (`ID_department`) REFERENCES `departments` (`ID_department`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `specialisations`
--
ALTER TABLE `specialisations`
  ADD CONSTRAINT `specialisations_ibfk_2` FOREIGN KEY (`ID_department`) REFERENCES `departments` (`ID_department`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`ID_group`) REFERENCES `groups` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
